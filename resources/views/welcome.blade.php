<html>
  <head>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- @TODO: replace SET_YOUR_CLIENT_KEY_HERE with your client key -->
    <script type="text/javascript"
      src="https://app.sandbox.midtrans.com/snap/snap.js"
      data-client-key="SB-Mid-client-eshaE1nm4ydfWXzq"></script>
      <meta name="csrf-token" content="{{ csrf_token() }}" />
      <script src="/assets/jquery/jquery.min.js"></script>
    <!-- Note: replace with src="https://app.midtrans.com/snap/snap.js" for Production environment -->
  </head>

  <body>
    <button id="pay-button">Pay!</button>

    <script type="text/javascript">
      // For example trigger on button clicked, or any time you need
      var payButton = document.getElementById('pay-button');
      payButton.addEventListener('click', function () {
        // Trigger snap popup. @TODO: Replace TRANSACTION_TOKEN_HERE with your transaction token
        window.snap.pay('{{$transaksi}}', {
          onSuccess: function(result){
            /* You may add your own implementation here */
            send_data(result);
          },
          onPending: function(result){
            /* You may add your own implementation here */
            send_data(result);
          },
          onError: function(result){
            /* You may add your own implementation here */
            send_data(result);
          },
          onClose: function(){
            /* You may add your own implementation here */
            alert('you closed the popup without finishing the payment');
          }
        })
      });

      function send_data(result)
      {
        $.ajax({
                        url: "/snap/transaksi",
                        method: "POST",
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        data: {'result' : result, 'nama' : "alvin"},
                        success: function(data) {
                    
                        },
                        error: function(data, exception){
                     
                        }
                      });
      }
    </script>
  </body>
</html>