@extends('admin.master')
@section('judul','Tabel Transaksi')
    
@section('konten')
<style>
    .select2-container {
        width: 100% !important;
        padding: 0;
    }
</style>
<meta name="csrf-token" content="{{ csrf_token() }}" />
<div class="container-fluid">

    <!-- Page Heading -->
    <h1 class="h3 mb-2 text-gray-800">Data Transaksi</h1>
    <p class="mb-4">Di halaman ini anda dapat melihat transaksi</a>.</p>

    <!-- DataTales Example -->
    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">Tabel</h6>
        </div>
        <div class="card-body">

       

            <div class="table-responsive">
                <table class="table table-bordered" id="dataTable2" width="100%" cellspacing="0">
                    <thead>
                        <tr>
                            <th>email</th>
                            <th>status</th>
                            <th>order id</th>
                            <th>harga</th>
                            <th>waktu</th>
                            <th>transfer</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tfoot>
                        <tr>
                            <th>email</th>
                            <th>status</th>
                            <th>order id</th>
                            <th>harga</th>
                            <th>waktu</th>
                            <th>transfer</th>
                            <th>Action</th>
                        </tr>
                    </tfoot>
                    <tbody>
                 
                    </tbody>
                </table>
            </div>
        </div>
    </div>

     <!-- Modal -->
     <div class="modal  fade" id="modal_carosel" tabindex="-1" role="dialog" aria-labelledby="modal_carosel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered  modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLongTitle">Transaksi</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            </div>
            <div class="modal-body">
                <form id="form_carosel">
                    <div class="mb-3">
                        <label for="produk" class="form-label">Nama Produk</label>
                        <input type="hidden"  class="form-control"  id="id" name="id" aria-describedby="judul">
                        <input type="text" disabled  class="form-control" required id="produk" name="produk" aria-describedby="judul">
                      </div>
                    <div class="mb-3">
                        <label for="id_game" class="form-label">ID Game</label>
                        <input type="text" disabled  class="form-control" required id="id_game" name="id_game" aria-describedby="judul">
                    </div>
                    <div class="mb-3">
                        <label for="status" class="form-label">status</label>
                        <input type="text" disabled  class="form-control" required id="status" name="status" aria-describedby="judul">
                    </div>
                    <div class="mb-3">
                        <label for="order_id" class="form-label">Order Id</label>
                        <input type="text" disabled  class="form-control" required id="order_id" name="order_id" aria-describedby="judul">
                    </div>
                    <div class="mb-3">
                        <label for="total" class="form-label">total</label>
                        <input type="text" disabled  class="form-control" required id="total" name="total" aria-describedby="judul">
                    </div>
                    <div class="mb-3">
                        <label for="nama" class="form-label">nama</label>
                        <input type="text" disabled  class="form-control" required id="nama" name="nama" aria-describedby="judul">
                    </div>
                    <div class="mb-3">
                        <label for="email" class="form-label">email</label>
                        <input type="text" disabled  class="form-control" required id="email" name="email" aria-describedby="judul">
                    </div>
                    <div class="mb-3">
                        <label for="no_telp" class="form-label">telpon</label>
                        <input type="text" disabled  class="form-control" required id="no_telp" name="no_telp" aria-describedby="judul">
                    </div>
                    <div class="mb-3">
                        <label for="transfer" class="form-label">transfer</label>
                        <input type="text" disabled  class="form-control" required id="transfer" name="transfer" aria-describedby="judul">
                    </div>
                    <div class="mb-3">
                        <label for="tanggal" class="form-label">tanggal</label>
                        <input type="text" disabled  class="form-control" required id="tanggal" name="tanggal" aria-describedby="judul">
                    </div>
                    @csrf
            </div>
            <div class="modal-footer justify-content-center">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">tutup</button>
            <button type="submit"  id="btn_acc" class="btn btn-primary">Transfer</button>
        </form>
            </div>
        </div>
        </div>
    </div>


</div>
    <script src="/assets/moment.min.js"></script>
    <script>
    $(document).ready(function() {

    var aksi_status = true;

    var table = $('#dataTable2').DataTable({
        ajax: {
            url: `/tabel-transaksi/data`,
            dataSrc: 'data',
        },
        columns: [
            {
                data: 'email',
            },
            {
                data: 'status',
            },
            {
                data: 'order_id',
            },
            {
                data: function(data, catatan, row) {
                        return formatRupiah(`${data.gross_amount}`, 'Rp. ');
         
                    },
            },
            {
                data: function(data, catatan, row) {
                        return moment(`${data.created_at}`).utc().format('DD-MM-YYYY HH:mm:ss')
                    },
            },
            {
                data: function(data, catatan, row) {
                                if(data.transfer == '0'){ return 'belom tranfer' }else{return 'sudah tranfer'}
                    },
            },
   
        ],         
         aoColumnDefs: [{
                targets: 6,
                data: 'id',
                "render": function(data, catatan, row) {
                      
                    return `
                        <a class="" href="#" 
                        id="btn_edit" data-id="${data}" 
                        data-produk="${row.produk}" 
                        data-status="${row.status}" 
                        data-order-id="${row.order_id}"
                        data-total="${row.gross_amount}" 
                        data-nama="${row.nama}" 
                        data-email="${row.email}" 
                        data-telp="${row.no_telp}" 
                        data-transfer="${row.transfer}" 
                        data-tanggal="${row.created_at}" 
                        data-id-game="${row.id_game}" 
                         ><i class="fas fa-edit" ></i></a>
                        `;
                        
                }
            }, ],
        dom: 'Bfrtip',
        buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print'
        ]
        
    });

    $('#dataTable2 tbody').on('click', '#btn_edit', function(e) {
            e.preventDefault();
            aksi_status = false;

            var id = this.getAttribute('data-id');
            var produk = this.getAttribute('data-produk');
            var status = this.getAttribute('data-status');
            var order_id = this.getAttribute('data-order-id');
            var total = this.getAttribute('data-total');
            var nama = this.getAttribute('data-nama');
            var email = this.getAttribute('data-email');
            var no_telp = this.getAttribute('data-telp');
            var transfer = this.getAttribute('data-transfer');
            var tanggal = this.getAttribute('data-tanggal');
            var id_game = this.getAttribute('data-id-game');

            if(transfer == 0){
                    var tranfer_2 = 'belom transfer';
                    $('#btn_acc').prop('disabled', false);   

                    if(status == 'settlement'){
                    $('#btn_acc').prop('disabled', false);   
                }else{
                    $('#btn_acc').prop('disabled', true);
                }
            }else{
                var tranfer_2 = 'sudah transfer';
                $('#btn_acc').prop('disabled', true);
                
            
            }
         
           
            $('#id').val(id);
            $('#produk').val(produk);
            $('#status').val(status);
            $('#order_id').val(order_id);
            $('#total').val(formatRupiah(total, 'Rp. '));
            $('#nama').val(nama);
            $('#email').val(email);
            $('#no_telp').val(no_telp);
            $('#transfer').val(tranfer_2);
            $('#id_game').val(id_game);
            $('#tanggal').val(moment(tanggal).utc().format('DD-MM-YYYY HH:mm:ss'));
            
            $('#modal_carosel').modal('show');
    });

    $('#form_carosel').submit(function(e){
        e.preventDefault();
        var data = new FormData(this);
        $.ajax({
                url: "/tabel-transaksi/store",
                method: "POST",
                data:  data,
                processData: false,
                contentType: false,
                success: function(data) {
                    table.ajax.reload();
                    $('#modal_carosel').modal('hide');
                    Toast.fire({
                        icon: 'success',
                        title: 'Simpan Berhasil'
                    });
                },
                error: function(data){
                    console.log(data);
                    Toast.fire({
                        icon: 'error',
                        title: data['responseJSON']['message']
                    });
                }
            });
    });

    /* Fungsi formatRupiah */
    function formatRupiah(angka, prefix){
    //    var angka2 = angka.toFixed(5);
    //    console.log(angka2);
        var number_string = angka.replace(/[^,.\d]/g, '').toString(),
        split   		= number_string.split(','),
        sisa     		= split[0].length % 3,
        rupiah     		= split[0].substr(0, sisa),
        ribuan     		= split[0].substr(sisa).match(/\d{3}/gi);

        // tambahkan titik jika yang di input sudah menjadi angka ribuan
        if(ribuan){
            separator = sisa ? '.' : '';
            rupiah += separator + ribuan.join('.');
        }

        rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
        return prefix == undefined ? rupiah : (rupiah ? 'Rp. ' + rupiah : '');
    }

    function clearData()
    {
        $('#id').val('');
        $('#produk').val('');
        $('#foto').val('');

    }
}); 
    </script>
@endsection