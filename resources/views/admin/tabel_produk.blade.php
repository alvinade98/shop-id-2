@extends('admin.master')
@section('judul','Tabel Produk')
    
@section('konten')
<style>
    .select2-container {
        width: 100% !important;
        padding: 0;
    }
</style>
<meta name="csrf-token" content="{{ csrf_token() }}" />
<div class="container-fluid">

    <!-- Page Heading -->
    <h1 class="h3 mb-2 text-gray-800">Data Produk</h1>
    <p class="mb-4">Di halaman ini anda dapat menambahkan data Produk</a>.</p>

    <!-- DataTales Example -->
    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">Tabel</h6>
        </div>
        <div class="card-body">

           <div class="d-flex justify-content-end mb-2 mt-2">
             <div><button type="button" class="btn btn-primary" id="tambah_carosel">Tambah Produk</button></div>
           </div>

            <div class="table-responsive">
                <table class="table table-bordered" id="dataTable2" width="100%" cellspacing="0">
                    <thead>
                        <tr>
                            <th>nama produk</th>
                            <th>deskripsi</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tfoot>
                        <tr>
                            <th>nama produk</th>
                            <th>deskripsi</th>
                            <th>Action</th>
                        </tr>
                    </tfoot>
                    <tbody>
                 
                    </tbody>
                </table>
            </div>
        </div>
    </div>

     <!-- Modal -->
     <div class="modal  fade" id="modal_carosel" tabindex="-1" role="dialog" aria-labelledby="modal_carosel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered  modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLongTitle">Produk</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            </div>
            <div class="modal-body">
                <form id="form_carosel">
                    <div class="mb-3">
                        <label for="produk" class="form-label">Nama Produk</label>
                        <input type="hidden"  class="form-control"  id="id" name="id" aria-describedby="judul">
                        <input type="hidden"  class="form-control"  id="foto_lama" name="foto_lama" aria-describedby="judul">
                        <input type="text"  class="form-control" required id="produk" name="produk" aria-describedby="judul">
                      </div>
                    <div class="mb-3">
                        <label for="deskripsi" class="form-label">Deskripsi Produk</label>
                        <textarea class="form-control" name="deskripsi" id="deskripsi" rows="10" cols="80">
                        </textarea>
                    </div>
                    <div class="mb-3">
                        <label for="foto" class="form-label">Foto</label>
                        <input type="file"  class="form-control"  id="foto" name="foto" aria-describedby="judul">
                    </div>
                    @csrf
            </div>
            <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">tutup</button>
            <button type="submit" id="btn_simpan" class="btn btn-primary">Simpan</button>
        </form>
            </div>
        </div>
        </div>
    </div>


</div>
    <script src="/assets/moment.min.js"></script>
    <script>
       $(document).ready(function() {

        CKEDITOR.replace( 'deskripsi' );
    var aksi_status = true;

    var table = $('#dataTable2').DataTable({
        ajax: {
            url: `/tabel-produk/data`,
            dataSrc: 'data',
        },
        columns: [
            {
                data: 'nama_produk',
            },
            {
                data: 'deskripsi',
            }
   
        ],         
         aoColumnDefs: [{
                targets: 2,
                data: 'id',
                "render": function(data, catatan, row) {
                        return `
                        <a class="" href="#" id="btn_edit" data-id="${data}" data-produk="${row.nama_produk}" data-deskripsi="${row.deskripsi}" data-foto="${row.foto}"  ><i class="fas fa-edit" ></i></a>
                        <a class="" href="#" id="btn_deleted" data-id="${data}"  ><i class="fas fa-trash" ></i></a>
                        `;
                }
            }, ],
        dom: 'Bfrtip',
        buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print'
        ]
        
    });

        
    $('#dataTable2 tbody').on('click', '#btn_deleted', function(e) {
            e.preventDefault();
            var id = this.getAttribute('data-id');
            Swal.fire({
            title: 'Apa kamu yakin ingin hapus data ini?',
            text: "Data akan hilang setelah dihapus!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Ya hapus data ini!'
            }).then((result) => {
            if (result.isConfirmed) {
                $.ajax({
                        url: "/tabel-produk/hapus",
                        method: "POST",
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        data: {'id' : id},
                        success: function(data) {
                            table.ajax.reload();
                            $('#modal_carosel').modal('hide');
                            Toast.fire({
                                icon: 'success',
                                title: 'Berhasil hapus data'
                            });
                        },
                        error: function(data, exception){
                            Toast.fire({
                                icon: 'error',
                                title: exception
                            });
                        }
                      });
            }
            });
         
        });
    $('#dataTable2 tbody').on('click', '#btn_edit', function(e) {
            e.preventDefault();
            // clearData();
            aksi_status = false;

            var id = this.getAttribute('data-id');
            var produk = this.getAttribute('data-produk');
            var deskripsi = this.getAttribute('data-deskripsi');
            var foto = this.getAttribute('data-foto');

           
            $('#id').val(id);
            $('#produk').val(produk);
            $('#foto_lama').val(foto);
            CKEDITOR.instances['deskripsi'].setData(deskripsi);
            
            $('#modal_carosel').modal('show');
    });
    
    $('#tambah_carosel').click(function(e){
        e.preventDefault();
        aksi_status = true;
        $('#modal_carosel').modal('show');
        clearData();
    });
    $('#form_carosel').submit(function(e){
        e.preventDefault();
        var data = new FormData(this);
        if(aksi_status){
            var inputcatatan = CKEDITOR.instances['deskripsi'].getData();
            var data = new FormData(this);
            data.delete('deskripsi');
            data.append('deskripsi', inputcatatan);
            $.ajax({
                url: "/tabel-produk/add",
                method: "POST",
                data:  data,
                processData: false,
                contentType: false,
                success: function(data) {
                    table.ajax.reload();
                    $('#modal_carosel').modal('hide');
                    Toast.fire({
                        icon: 'success',
                        title: 'Simpan Berhasil'
                    });
                },
                error: function(data){
                    console.log(data);
                    Toast.fire({
                        icon: 'error',
                        title: data['responseJSON']['message']
                    });
                }
            });
        }else{
            var inputcatatan = CKEDITOR.instances['deskripsi'].getData();
            var data = new FormData(this);
            data.delete('deskripsi');
            data.append('deskripsi', inputcatatan);
            $.ajax({
                url: "/tabel-produk/edit",
                method: "POST",
                data:  data,
                processData: false,
                contentType: false,
                success: function(data) {
                    table.ajax.reload();
                    $('#modal_carosel').modal('hide');
                    Toast.fire({
                        icon: 'success',
                        title: 'Simpan Berhasil'
                    });
                },
                error: function(data){
                    Toast.fire({
                        icon: 'error',
                        title: data['responseJSON']['message']
                    });
                }
            });
        }
    

    });
    
    function clearData()
    {
        $('#id').val('');
        $('#produk').val('');
        $('#foto').val('');
        CKEDITOR.instances['deskripsi'].setData('');

    }
}); 
    </script>
@endsection