@extends('home.master')
@section('konten')
    <script type="text/javascript"
    src="https://app.sandbox.midtrans.com/snap/snap.js"
    data-client-key="SB-Mid-client-eshaE1nm4ydfWXzq"></script>
    <meta name="csrf-token" content="{{ csrf_token() }}" />
  <div class="container mtp-1">
  
  </div>
  <script type="text/javascript">
    // For example trigger on button clicked, or any time you need
    addEventListener('load', function () {
      // Trigger snap popup. @TODO: Replace TRANSACTION_TOKEN_HERE with your transaction token
      window.snap.pay('{{$transaksi}}', {
        onSuccess: function(result){
          /* You may add your own implementation here */
          send_data(result);
        },
        onPending: function(result){
          /* You may add your own implementation here */
          send_data(result);
        },
        onError: function(result){
          /* You may add your own implementation here */
          send_data(result);
        },
        onClose: function(){
          /* You may add your own implementation here */
          alert('you closed the popup without finishing the payment');
        }
      })
    });

    function send_data(result)
    {
      $.ajax({
                      url: "/transaksi/action",
                      method: "POST",
                      headers: {
                          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                      },
                      data: {'result' : result, 'nama' : `{{$nama}}`, 'email' : `{{$email}}`, 'no_telp' : `{{$no_telp}}`, 'produk' : `{{$produk}}`, 'id_game' : `{{$id_game}}`},
                      success: function(data) {
                        
                        if(data.data == 'sukses'){
                          location.replace("http://localhost:8000/")
                        }else{
                          location.replace("http://localhost:8000/")
                        }
                      },
                      error: function(data, exception){
                   
                      }
                    });
    }
  </script>
  <br>
  <br>
  <br>
  <br>
  <br>
  <br>
  <br>
  <br>
  <br>
  <br>
  <br>
  <br>
  <br>
  <br>
  <br>
  <br>
  <br>
  <br>
  <br>
@endsection