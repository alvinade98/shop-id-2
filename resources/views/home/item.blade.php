@extends('home.master')
@section('konten')
  <div class="container mtp-1">
    <div class="row ">
      <div class="col-md-4 mt-4">
        <img src="/storage/{{$produk->foto}}"  alt="" class="img-fluid">
          <div class="judul-item mt-2">
            {{$produk->nama_produk}}
          </div>
          <div class="item-conten mt-2">
            {!!$produk->deskripsi!!}
          </div>
      </div>
      <div class="col-md-8 mt-4">

          <div class="border-item-1">
              <div class="judul-item-1 ps-3 pt-2">
                  Pilih voucher
              </div>
              <div class="row m-3 d-flex justify-content-md-center justify-content-center" id="item">

 
                @foreach ($vocher as $item)
                  <div class="card item-data   m-3" id="denomination_{{$item->id}}"  style="width: 12rem; cursor: pointer" onclick="clik({{$item->id}})">
                    <div class="card-body">
                      <p class="card-text text-center" id="item_{{$item->id}}">{{$item->satuan}}</p>
                      <p class="card-text text-center "  id="harga_{{$item->id}}" >{!!	$hasil_rupiah = "Rp " . number_format($item->harga,2,',','.');  !!}</p>
                    </div>
                  </div>
                @endforeach
              </div>
          </div>

     


        <!-- Modal -->
      <div class="modal fade" id="modal_bayar" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="exampleModalLabel">Bayar</h5>
              <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <form action="/transaksi" method="get">
                <div class="mb-3">
                    <label for="id_game" class="form-label">ID Game</label>
                    <input type="text" required  class="form-control"  id="id_game" name="id_game" aria-describedby="judul">
                </div>
                <div class="mb-3">
                    <label for="nama" class="form-label">Nama</label>
                    <input type="hidden" required  class="form-control"  id="id" name="id" aria-describedby="judul">
                    <input type="text" required  class="form-control"  id="nama" name="nama" aria-describedby="judul">
                </div>
                <div class="mb-3">
                    <label for="email" class="form-label">Email</label>
                    <input type="email" required  class="form-control"  id="email" name="email" aria-describedby="judul">
                </div>
                <div class="mb-3">
                    <label for="no_telp" class="form-label">No. telpon</label>
                    <input type="number" required  class="form-control"  id="no_telp" name="no_telp" aria-describedby="judul">
                    <input type="hidden"  required   class="form-control"  id="item_data" name="item_data" aria-describedby="judul">
                    <input type="hidden"  required    class="form-control"  id="total" name="total" aria-describedby="judul">
                    <input type="hidden"  required  value="{{$produk->nama_produk}}"   class="form-control"  id="produk" name="produk" aria-describedby="judul">
                </div>
                @csrf
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
              <button type="submit" class="btn btn-primary">Bayar</button>
            </form>
            </div>
          </div>
        </div>
      </div>

      </div>
    </div>
  </div>
  <script>
    var hover ;
   function clik(id){
     $( `#denomination_${hover}` ).removeClass( "active" );
     hover = id;
     $( `#denomination_${id}` ).addClass( "active" );

     var item = $( `#item_${id}` ).html();
     var harga = $( `#harga_${id}` ).html();
      $('#item_data').val(item);
      $('#total').val(harga);
      $('#id').val(id);

     $('#modal_bayar').modal('show');
   }


     /* Fungsi formatRupiah */
		function formatRupiah(angka, prefix){
			var number_string = angka.replace(/[^,\d]/g, '').toString(),
			split   		= number_string.split(','),
			sisa     		= split[0].length % 3,
			rupiah     		= split[0].substr(0, sisa),
			ribuan     		= split[0].substr(sisa).match(/\d{3}/gi);
 
			// tambahkan titik jika yang di input sudah menjadi angka ribuan
			if(ribuan){
				separator = sisa ? '.' : '';
				rupiah += separator + ribuan.join('.');
			}
 
			rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
			return prefix == undefined ? rupiah : (rupiah ? 'Rp. ' + rupiah : '');
		}
  </script>
@endsection