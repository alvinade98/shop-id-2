<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Shop Id</title>
    <link rel="stylesheet" href="/bootstrap/css/bootstrap.css">
    <link rel="stylesheet" href="/assets/css/style.css">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Roboto+Slab:wght@300;400;600;700;800;900&family=Signika+Negative:wght@300;500;600;700&display=swap" rel="stylesheet">
    <script src="/assets/jquery/jquery.min.js"></script>
</head>
    <body>
      <nav class="navbar navbar-expand-lg  fixed-top">
        <div class="container-fluid">
          <a class="navbar-brand" href="#">
            {{-- <img src="/docs/5.3/assets/brand/bootstrap-logo.svg" alt="Logo" width="30" height="24" class="d-inline-block align-text-top"> --}}
            Shop Id
          <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
          </button>
        </a>
          <div class="collapse navbar-collapse" id="navbarSupportedContent">      
        </div>
        </div>
      </nav>
      
        @yield('konten')

        <footer>
            <div class="container">
              <div class="row">
                <div class="col-md-12">
                  <div class="font-footer">
                     Top up game terpercaya
                  </div>
                </div>
              </div>
            </div>
        </footer>
    </body>
</html>
<script src="/assets/sweetalert.min.js"></script>
<script>
      const Toast = Swal.mixin({
    toast: true,
    position: 'top-end',
    showConfirmButton: false,
    showCloseButton: true,
    timer: 5000,
    timerProgressBar: true,
    didOpen: (toast) => {
      toast.addEventListener('mouseenter', Swal.stopTimer)
      toast.addEventListener('mouseleave', Swal.resumeTimer)
    }
    }); 

    

</script>
    @if (session()->has('status'))
       <script>
              Toast.fire({
                      icon: `{{session('icon')}}`,
                      title: `{{session('status')}}`
                    });
       </script>
    @endif
   
<script src="/bootstrap/js/bootstrap.js"></script>
