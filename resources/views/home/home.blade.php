@extends('home.master')
@section('konten')
<div id="carouselExample" class="carousel slide mb-4">
    <div class="carousel-inner">
      <div class="carousel-item active">
        <img src="/image/ml.jpg" class="d-block w-100 "  alt="...">
      </div>
      <div class="carousel-item">
        <img src="/image/pubg.jpeg" class="d-block w-100"  alt="...">
      </div>
      <div class="carousel-item">
        <img src="/image/ml.jpg" class="d-block w-100" alt="...">
      </div>
    </div>
    <button class="carousel-control-prev" type="button" data-bs-target="#carouselExample" data-bs-slide="prev">
      <span class="carousel-control-prev-icon" aria-hidden="true"></span>
      <span class="visually-hidden">Previous</span>
    </button>
    <button class="carousel-control-next" type="button" data-bs-target="#carouselExample" data-bs-slide="next">
      <span class="carousel-control-next-icon" aria-hidden="true"></span>
      <span class="visually-hidden">Next</span>
    </button>
  </div>

  <div class="container mb-4 ">
    <div class="row">
      <div class="col-md-12">
          <div class="font-list">
            List Game
          </div>
      </div>
    </div>
  </div>

    <div class="container">
      <div class="row text-center">

        @foreach ($produk as $item)
          <div class="col-sm-4 col-md-4 mt-3 d-flex justify-content-center">
            <div class="card" style="width: 18rem;">
              <img src="/storage/{{$item->foto}}" class="card-img-top" alt="...">
              <div class="card-body">
                <h5 class="card-title text-center">{{$item->nama_produk}}</h5>
                <p class="card-text">{!!$item->deskripsi!!}</p>
              </div>
              <div class="card-footer text-center">
                <a href="/item/{{$item->id}}" class="btn btn-primary">Beli</a>
              </div>
            </div>
          </div>

        @endforeach

 



      </div>
    </div>

    {{-- <div class="container mt-4">
      <div class="row">
        <div class="col-md-12">
          <div class="font-list">
            Populer Game
          </div>
        </div>
      </div>
    </div>
    
    <div class="container mt-4 mb-4">
      <div class="row">
        
        <div class="col-sm-4 col-md-4 mt-3 d-flex justify-content-center">
          <div class="card" style="width: 18rem;">
            <img src="/image/ml.jpg" class="card-img-top" alt="...">
            <div class="card-body">
              <h5 class="card-title text-center">Mobile Lagend</h5>
              <p class="card-text">Lorem ipsum dolor sit amet consectetur adipisicing elit. Aut expedita alias id ducimus rerum itaque mollitia pariatur? Id, rerum molestiae ipsum soluta impedit minima aut! Possimus odio expedita dolor ab.</p>
            </div>
            <div class="card-footer text-center">
              <a href="#" class="btn btn-primary">Beli</a>
            </div>
          </div>
        </div>


        <div class="col-sm-4  col-md-4 mt-3 d-flex justify-content-center">
          <div class="card" style="width: 18rem;">
            <img src="/image/pubg.jpeg" class="card-img-top" alt="...">
            <div class="card-body">
              <h5 class="card-title text-center">Pubg</h5>
              <p class="card-text">Lorem ipsum dolor sit amet consectetur adipisicing elit. Aut expedita alias id ducimus rerum itaque mollitia pariatur? Id, rerum molestiae ipsum soluta impedit minima aut! Possimus odio expedita dolor ab.</p>
            </div>
            <div class="card-footer text-center">
              <a href="#" class="btn btn-primary">Beli</a>
            </div>
          </div>
        </div> --}}

        
      </div>
    </div>
@endsection