<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Home;
use App\Http\Controllers\Login;
use App\Http\Controllers\admin\Dasboard;
use App\Http\Controllers\admin\C_produk;
use App\Http\Controllers\admin\C_vocher;
use App\Http\Controllers\admin\C_transaksi;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Route::get('/', [Home::class, 'index'])->name('home');
Route::get('/item/{id}', [Home::class, 'item']);
// Route::get('/snap', [Home::class, 'snap_test']);
Route::post('/transaksi/action', [Home::class, 'order_transaksi']);
Route::get('/transaksi', [Home::class, 'order_pay']);

Route::middleware(['guest', 'revalidate'])->group(function () {
Route::get('/login', [Login::class, 'view_login'])->name('login');
Route::post('/login', [Login::class, 'action_login']);
// Route::get('/daftar', [Login::class, 'view_daftar']);
// Route::post('/daftar', [Login::class, 'action_daftar']);

});

Route::get('/logout', [Login::class, 'logout']);



Route::middleware(['checkrole:superadmin', 'revalidate'])->group(function () {
    
    Route::get('/dashboard', [Dasboard::class, 'view_dasboard']);
    Route::get('/tabel-produk', [C_produk::class, 'view_tabel']);
    Route::get('/tabel-produk/data', [C_produk::class, 'get_data']);
    Route::post('/tabel-produk/add', [C_produk::class, 'add_produk']);
    Route::post('/tabel-produk/edit', [C_produk::class, 'edit_produk']);
    Route::post('/tabel-produk/hapus', [C_produk::class, 'delete']);

    Route::get('/tabel-vocher', [C_vocher::class, 'view_tabel_vocher']);
    Route::get('/tabel-vocher/data', [C_vocher::class, 'get_Data_vocher']);
    Route::post('/tabel-vocher/add', [C_vocher::class, 'tambah_produk']);
    Route::post('/tabel-vocher/edit', [C_vocher::class, 'edit_vocher']);
    Route::post('/tabel-vocher/hapus', [C_vocher::class, 'delete']);
    
    Route::get('/tabel-transaksi', [C_transaksi::class, 'tabel_data']);
    Route::get('/tabel-transaksi/data', [C_transaksi::class, 'get_data']);
    Route::post('/tabel-transaksi/store', [C_transaksi::class, 'data_update']);
});