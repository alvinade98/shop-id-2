<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Jenis_vocher extends Model
{
    protected $table = 'jenis_vochers';

    protected $fillable = [
        'id_produk',
        'satuan',
        'harga',
        'is_active',
    ];
}
