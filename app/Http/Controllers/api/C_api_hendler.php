<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Transaksi;

class C_api_hendler extends Controller
{
    function payment_hendler(Request $request)
    {
        $json = json_decode($request->getContent());

        $signature_key = hash('sha512',$json->order_id . $json->status_code . $json->gross_amount . env('MIDTRANS_SERVER_KEY')); 

        if($signature_key != $json->signature_key){
            return abort('404');
        }

        if($json->transaction_status  == 'settlement'){
            print_r('kirim email sukses');
        }

        Transaksi::where('order_id', $json->order_id)->update([
            'status' => $json->transaction_status
        ]);
    }
}
