<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;

class Login extends Controller
{
    function view_login()
    {
        return view('login');
    }

    function action_login(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'g-recaptcha-response' => 'required',
        ]);
 
        if ($validator->fails()) {
            return back()
                        ->with('captcha','capthca harus di isi');
        }
        $credentials = $request->validate([
            'email' => ['required', 'email'],
            'password' => ['required'],
        ]);

        $data2 = user::where('email', $request->input('email'))->count();

        if($data2 != 0){

            $data = user::where('email', $request->input('email'))->first();
     
    
                if($data->is_active == '1'){
                    if (Auth::attempt($credentials)) {
                        $request->session()->regenerate();
            
                        return redirect()->intended('/dashboard');
                    }
                }
            }
            return back()  ->with('captcha','username atau password salah!');
    }

    function view_daftar()
    {
        return view('daftar');
    }
    function action_daftar(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'g-recaptcha-response' => 'required',
        ]);
 
        if ($validator->fails()) {
            return back()
                        ->with('captcha','capthca harus di isi');
        }
        $request->validate([
            'email' => ['required', 'email'],
            'password' => ['required'],
            'nama' => ['required'],
            'no_telp' => ['required'],
        ]);
        $data2 = user::where('email', $request->input('email'))->count();

        if($data2 == 0){

            User::insert([
                'name' => $request->input('nama'),
                'email' => $request->input('email'),
                'no_telp' => $request->input('no_telp'),
                'password' => Hash::make($request->input('password')),
            ]);
               return redirect('/login');

            }
               return back()  ->with('captcha','email sudah ada!');
    }

    function logout(Request $request)
    {
        Auth::logout();

        $request->session()->invalidate();

        $request->session()->regenerateToken();

        return redirect('/');
    }
}
