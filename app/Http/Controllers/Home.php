<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Produk;
use App\Models\Jenis_vocher;
use App\Models\Transaksi;

class Home extends Controller
{
    public function index(Request $request)
    {
        // $request->session()->push('status', 'developers');
        $produk = Produk::get();
        $data_array = [
            'produk' => $produk
        ];

        return  view('home/home', $data_array);
    }

    public function item(Request $request, $id)
    {
        $produk = Produk::where('id',$id)->first();
        $vocher = Jenis_vocher::where('id_produk','=',$id)
                                ->orderBy('harga', 'asc')
                                ->get();

        $data_array = [
            'produk' => $produk,
            'vocher' => $vocher,
        ];

        return  view('home/item',$data_array);
    }


    function order_pay(Request $request)
    {

      $request->validate([
            'nama' => ['required'],
            'email' => ['required'],
            'no_telp' => ['required'],
            'id_game' => ['required'],
        ]);

 
        $data = Jenis_vocher::where('id', $request->id)->first();
        // print_r($data->harga);
        // die();
                 // Set your Merchant Server Key
    \Midtrans\Config::$serverKey = env('MIDTRANS_SERVER_KEY');
    // Set to Development/Sandbox Environment (default). Set to true for Production Environment (accept real transaction).
    \Midtrans\Config::$isProduction = false;
    // Set sanitization on (default)
    \Midtrans\Config::$isSanitized = true;
    // Set 3DS transaction for credit card to true
    \Midtrans\Config::$is3ds = true;

    $params = array(
        'transaction_details' => array(
            'order_id' => rand(),
            'gross_amount' => $data->harga,
        ),
        'customer_details' => array(
            'first_name' => $request->nama,
            'email' => $request->email,
            'phone' => $request->no_telp,
        ),
        'item_details' => array(
            [
                'id' => $request->id,
                'price' => $data->harga,
                'quantity' => '1',
                'name' => $data->satuan,
            ]       
        ),
 
    );

      $snapToken = \Midtrans\Snap::getSnapToken($params);

      return view('home.bayar',['transaksi' => $snapToken, 'nama' => $request->nama, 'email' => $request->email, 'no_telp' => $request->no_telp, 'produk' => $request->produk , 'id_game' => $request->id_game]);
    }

    
    function order_transaksi(Request $request)
    {

        $data1 = json_encode($request->result);
        $data = json_decode($data1);
    
        if($data->status_code == 200 || $data->status_code == 201){

        
        $status = Transaksi::insert([
            'id_game' => $request->id_game,
            'status' =>    isset($data->transaction_status) ? $data->transaction_status : 'null',
            'transaction_id' => $data->transaction_id,
            'order_id' => $data->order_id,
            'gross_amount' => $data->gross_amount,
            'payment_type' => $data->payment_type,
            'payment_code' => isset($data->payment_code) ? $data->payment_code : 'null' ,
            'pdf_url' => isset($data->pdf_url) ? $data->pdf_url : 'null',
            'nama' => $request->nama,
            'email' => $request->email,
            'no_telp' => $request->no_telp,
            'produk' => $request->produk,
        ]);

        if($status == true){
            $request->session()->flash('status', 'pembayaran di proses!');
            $request->session()->flash('icon', 'success');
            return response()->json([
                'success' => true,
                'message' => 'Pembelian berhasil',
                'data'    => 'sukses'
            ],200);
            
        }else{
            $request->session()->flash('status', 'pembayaran gagal!');
            $request->session()->flash('icon', 'error');
            return response()->json([
                'success' => true,
                'message' => 'Pembelian gagal',
                'data'    => 'error'
            ],200);


        }
      }else{
        $request->session()->flash('status', 'pembayaran gagal!');
        $request->session()->flash('icon', 'error');
        return response()->json([
            'success' => true,
            'message' => 'Pembayaran gagal',
            'data'    => 'error'
        ],200);
      }
    }
}
