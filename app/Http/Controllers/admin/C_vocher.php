<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Jenis_vocher;
use App\Models\Produk;

class C_vocher extends Controller
{
    function view_tabel_vocher()
    {
        $data = Produk::get();
        $dataArray = [
            'produk' => $data
        ];
        return view('admin.tabel_vocher',$dataArray);
    }
    function get_Data_vocher()
    {
        $data = Jenis_vocher::join('produk','produk.id','=','jenis_vochers.id_produk')
                    ->where('jenis_vochers.is_active','=','1')
                    ->select('jenis_vochers.*','produk.nama_produk')
                    ->get();
                
         return response()->json([
            'success' => true,
            'message' => 'berhasil ambil data',
            'data'    => $data 
        ],200);    
    }
    function tambah_produk(Request $request)
    {   
        $request->validate([
            'nama_produk' => ['required'],
            'satuan' => ['required'],
            'harga' => ['required'],
        ]);
        $result = preg_replace("/[^0-9]/", "", $request->harga);

        Jenis_vocher::insert([
            'id_produk' => $request->nama_produk,
            'satuan' => $request->satuan,
            'harga' => $result,
        ]);
        
        return response()->json([
            'success' => true,
            'message' => 'berhasil simpan data',
            'data'    => 'berhasil simpan data'
        ],200);
    }
    function edit_vocher(Request $request)
    {
        $request->validate([
            'nama_produk' => ['required'],
            'satuan' => ['required'],
            'harga' => ['required'],
        ]);
        $result = preg_replace("/[^0-9]/", "", $request->harga);

        Jenis_vocher::where('id', $request->id)
                    ->update([
                        'id_produk' => $request->nama_produk,
                        'satuan' => $request->satuan,
                        'harga' => $result,
                    ]);
        return response()->json([
            'success' => true,
            'message' => 'berhasil update data',
            'data'    => 'berhasil update data'
        ],200);
    }
    function delete(Request $request)
    {
        Jenis_vocher::where('id', $request->id)
        ->update([
            'is_active' => '0',
        ]);
        return response()->json([
            'success' => true,
            'message' => 'berhasil update data',
            'data'    => 'berhasil update data'
        ],200);
    }
}
