<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Produk;
use Illuminate\Support\Facades\File;

class C_produk extends Controller
{
    function view_tabel()
    {
        return view('admin.tabel_produk');
    }
    function get_data()
    {
         $data = Produk::where('is_active','=','1')->get();

         return response()->json([
            'success' => true,
            'message' => 'berhasil ambil data',
            'data'    => $data 
        ],200);
    }
    function add_produk(Request $request)
    {
        $file_status = $_FILES["foto"]["name"];

        $request->validate([
            'produk' => ['required'],
            'deskripsi' => ['required'],
            'foto' => ['image','mimes:jpg,png,jpeg,gif,svg','max:2048'],
        ]);

        if($file_status){

            $image_path = $request->file('foto')->store('image/produk', 'public');
            
            Produk::insert([
                'nama_produk' => $request->produk,
                'deskripsi' => $request->deskripsi,
                'foto' =>  $image_path,
            ]);

            return response()->json([
                'success' => true,
                'message' => 'berhasil simpan data',
                'data'    => 'berhasil simpan data'
            ],200);

          }else{

            return response()->json([
                'success' => true,
                'message' => 'error',
                'data'    => 'gagal'
            ],401);

          }

    }
    function edit_produk(Request $request)
    {

        $file_status = $_FILES["foto"]["name"];
        $file_lama =  $request->input('foto_lama');

        $request->validate([
            'produk' => ['required'],
            'deskripsi' => ['required'],
            'foto' => ['image','mimes:jpg,png,jpeg,gif,svg','max:2048'],
        ]);

        if($file_status)
        {
            if(File::exists(public_path('storage/'.$file_lama))){
                File::delete(public_path('storage/'.$file_lama));

                $image_path = $request->file('foto')->store('image/produk', 'public');

                Produk::where('id', $request->id)
                ->update([
                    'nama_produk' => $request->produk,
                    'deskripsi' => $request->deskripsi,
                    'foto' =>  $image_path,
            ]);
           
            return response()->json([
                'success' => true,
                'message' => 'berhasil update data',
                'data'    => 'berhasil update data'
            ],200);

            }else{

                Produk::where('id', $request->id)
                ->update([
                    'nama_produk' => $request->produk,
                    'deskripsi' => $request->deskripsi,
               ]);
           
                return response()->json([
                    'success' => true,
                    'message' => 'berhasil update data',
                    'data'    => 'berhasil update data'
                ],200);

            }

        }else{
            Produk::where('id', $request->id)
            ->update([
                'nama_produk' => $request->produk,
                'deskripsi' => $request->deskripsi,
           ]);
       
            return response()->json([
                'success' => true,
                'message' => 'berhasil update data',
                'data'    => 'berhasil update data'
            ],200);
        }
    
    }
    function delete(Request $request){
        
        Produk::where('id', $request->id)
        ->update(['is_active' => '0']);

        return response()->json([
            'success' => true,
            'message' => 'berhasil update data',
            'data'    => 'berhasil update data'
        ],200);
    }
}
