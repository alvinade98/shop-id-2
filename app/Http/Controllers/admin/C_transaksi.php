<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Transaksi;

class C_transaksi extends Controller
{
    function tabel_data()
    {
        return view('admin.tabel_transaksi');
    }

    function get_data()
    {
        $data = Transaksi::get();

        return response()->json([
            'success' => true,
            'message' => 'berhasil ambil data',
            'data'    => $data 
        ],200);
    }

    function data_update(Request $request)
    {
        $status =  Transaksi::where('id', $request->id)->first();

        if($status->status == 'settlement'){
        Transaksi::where('id', $request->id)->update([
            'transfer' => '1'
        ]);

            return response()->json([
                'success' => true,
                'message' => 'update data',
                'data'    => ''
            ],200);
        }
            
            return response()->json([
                'success' => true,
                'message' => 'error update data',
                'data'    => 'error'
            ],401);
    }
}
