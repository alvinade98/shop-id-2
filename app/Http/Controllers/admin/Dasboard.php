<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Transaksi;

class Dasboard extends Controller
{
    function view_dasboard()
    {
       $jumlah_pending = Transaksi::where('status','pending')->count();
       $jumlah_settlement = Transaksi::where('status','settlement')->count();
       $jumlah_sukses = Transaksi::where(['status' => 'settlement', 'transfer' => '1'])->count();
       $jumlah_belom = Transaksi::where(['status' => 'settlement', 'transfer' => '0'])->count();

    //    print_r($jumlah_sukses);
    //    die;

       $data_array = [
            'pending' => $jumlah_pending,
            'settlement' => $jumlah_settlement,
            'sukses_transfer' => $jumlah_sukses,
            'sukses_belom_transfer' => $jumlah_belom,
       ];

        return view('admin.dasboard', $data_array);
    }
}
